package com.demo.controller;

import com.apidoc.annotation.Api;
import com.demo.bean.Result;
import com.demo.bean.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 用以测试请求参数的类型
 */
@Api("测试请求参数类型")
@RestController
@RequestMapping("/testRequestParamType")
public class TestRequestParamTypeController {

//    //-----请求参数类型--------
//    public static final String URL = "URL拼接参数 (示例: ?a=XX&&b=XX)";
//    public static final String URI = "URI占位符 (示例: /XXX/{id}/{name})"
//    public static final String JSON = "JSON类型数据";
//    public static final String FROM = "FROM表单数据";
    //没有类型

    @RequestMapping(value = "/none")
    public Result none() {
        return Result.success();
    }

    @GetMapping(value = "/url")
    public Result url() {
        return Result.success();
    }

    @GetMapping(value = "/urlParam")
    public Result urlParam(int a) {
        return Result.success();
    }


    @GetMapping(value = "/urlParams")
    public Result urlParams(int a, String b) {
        return Result.success();
    }


    @GetMapping(value = "/uri/{a}")
    public Result uri(@PathVariable("a") int a) {
        return Result.success();
    }

    @GetMapping(value = "/uri/{a}/{b}")
    public Result uriParams(@PathVariable("a") int a, @PathVariable int b) {
        return Result.success();
    }

    @PostMapping(value = "/json")
    public Result json(@RequestBody User user) {
        return Result.success();
    }

    @PostMapping(value = "/file")
    public Result file(MultipartFile file) {
        return Result.success(file.getOriginalFilename());
    }

    @PostMapping(value = "/files")
    public Result files(MultipartFile[] files) {
        return Result.success(files.length);
    }

}
