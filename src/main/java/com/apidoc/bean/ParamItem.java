package com.apidoc.bean;

import java.util.List;

/**
 * 参数
 * 请求参数和响应参数的组成部分
 */
public class ParamItem {
    /**
     * 参数名称
     */
    private String name;

    /**
     * 数据类型
     */
    private String dataType;

    /**
     * 描述
     */
    private String description = "";

    /**
     * 默认值
     */
    private Object defaultValue;

    /**
     * 是否必须
     */
    private boolean required = true;//默认为是

    /**
     * 所属于哪个对象
     * 用作对象的自嵌套或互相嵌套
     */
    private String pid = "0";

    //组装tree结构
    private List<ParamItem> list;

    private String parentClassName;//父类名称

    /**
     * 是否显示
     */
    private boolean show = true;//默认显示

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<ParamItem> getList() {
        return list;
    }

    public void setList(List<ParamItem> list) {
        this.list = list;
    }

    public String getParentClassName() {
        return parentClassName;
    }

    public void setParentClassName(String parentClassName) {
        this.parentClassName = parentClassName;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }
}

