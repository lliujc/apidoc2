package com.apidoc.bean;

/**
 * 方法 bean
 */
public class Action {
    private String methodName;//方法名称
    private String name;//名称
    private Integer order;//排序

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
