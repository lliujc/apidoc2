package com.apidoc.common;

import java.util.HashMap;

/**
 * 封装Map, 方便易用
 * 提供 get和set方式,提供自动强制类型转化
 *
 * @author : peng.liu
 * @date : 2018/5/9 13:32
 * @see HashMap
 */
public class VO extends HashMap<String,Object> {

    public VO() {
        super();
    }

    public VO(int size) {
        super(size);
    }

    public VO set(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public <T> T get(String key) {
        return (T) super.get(key);
    }

    public String getStr(String key) {
        return super.get(key).toString();
    }


}
