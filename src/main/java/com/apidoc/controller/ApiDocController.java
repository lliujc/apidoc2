package com.apidoc.controller;

import com.alibaba.fastjson.JSONObject;
import com.apidoc.bean.Action;
import com.apidoc.bean.Detail;
import com.apidoc.bean.Module;
import com.apidoc.common.Const;
import com.apidoc.common.VO;
import com.apidoc.utis.GeneratorUtil;
import com.apidoc.utis.JsonUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文档生成controller
 */
@RestController
@RequestMapping("/apidoc")
public class ApiDocController {

    //是否开启文档功能
    @Value("${apidoc}")
    private boolean openApiDoc;

    /**
     * 获取文档基本信息
     */
    @GetMapping("/info")
    public Object info() {
        if (openApiDoc) {
            return GeneratorUtil.getInfo();
        } else {
            return "文档未开启";
        }
    }

    /**
     * 获取模块信息
     */
    @GetMapping("/modules")
    public List<Module> modules(String packageName) {
        return GeneratorUtil.getModules(packageName);
    }

    /**
     * 获取接口列表信息
     * 也就是获取public修饰的方法 信息
     * 根据模块名称获取该模块下所有类的public 的方法 信息
     */
    @GetMapping("/actions")
    public List<Action> methods(String moduleName) {
        return GeneratorUtil.getMethods(moduleName);
    }


    @GetMapping("/detail")
    public Detail detail(String methodUUID) {
        return GeneratorUtil.getDetail(methodUUID);
    }

    /**
     * 修改信息文档基本信息
     */
    @PostMapping("/updateInfo")
    public boolean updateInfo(@RequestBody String content) {
        return JsonUtil.writer(Const.info, content);
    }

    /**
     * 修改信息文档基本信息
     */
    @PostMapping("/updateAction")
    public boolean updateAction(@RequestBody VO vo) {
        String moduleName = vo.getStr("moduleName");
        String methodName = vo.getStr("methodName");
        String actionName = vo.getStr("actionName");
        String path = Const.actionFile + moduleName + ".json";//保存的路径
        //读取文件内容
        JSONObject readerObject = JsonUtil.readerObject(path);
        //修改action的名称
        JSONObject action = readerObject.getJSONObject(methodName);
        action.put("name", actionName);
        readerObject.put(methodName, action);
        return JsonUtil.writer(path, readerObject);
    }

    /**
     * 修改接口描述信息
     */
    @PostMapping("/updateActionDescription")
    public boolean updateActionDescription(@RequestBody VO vo) {
        String fileName = vo.get("fileName");
        String description = vo.get("description");
        return JsonUtil.writer(Const.actionDescription + fileName, description);
    }

    /**
     * 修改接口详情
     */
    @PostMapping("/updateDetail")
    public boolean updateDetail(@RequestBody VO vo) {
        String path = Const.actionParams + vo.get("methodUUID") + ".json";
        String content = JsonUtil.toJsonString(vo.get("update"));
        return JsonUtil.writer(path, content);
    }


}



